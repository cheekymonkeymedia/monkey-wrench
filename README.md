#Monkey Wrench

_**version**_: v2.2

_**maintainer**: Calvin Tyndall (sleepingmonk) calvin@cheekymonkeymedia.ca_

##Description

Monkey Wrench is a set of scripts and default files for automating various tasks in our development workflow.

##Requirements

###Bash Shell

Windows users should be running Windows 10 Pro to be able to use Docker for Win.

NOTE: Ubuntu Bash Shell is not the correct advice. It would require installing docker inside ubuntu instead of Docker for Win.
Windows people tell me GIT BASH is the way to go.  https://git-for-windows.github.io

The Monkey Wrench scripts are bash scripts that execute bash commands. Cygwyn or other Windows workaround shells may not work.

NOTE: Working a few kinks out to run mw from inside the docker container instead, if needed.  To access:

`docker-compose exec php sh` will bring you to a shell prompt inside the container.~

###git-2.0 or higher

Monkey Wrench scripts like `mw deploy [env]` use many git commands. We assume at least git 2.0 for two main reasons:
- `git push` defaults to the currently checked out branch.
- support for git tag --sort=v:refname for managing pantheon tags.


###Terminus 1.0  (Recommended)

This is not required but...

`mw setup` will use terminus to set up our multidev environments. It will skip this part if you do not have terminus installed. You will then need to manually click 2 buttons from the pantheon dashboard.

In the future we will probably leverage terminus more.  Plus you may find it useful for general site management on Pantheon.

If you install with composer, make sure you install it somewhere globally and not in a specific project directory.

https://github.com/pantheon-systems/terminus


##Installation

Clone monkey-wrench.  You can clone it anywhere but I recommend beside or higher than your project directories.

i.e.
```
~/Sites/monkey-wrench
~/Sites/project-1
~/sites/project-2
...
```
`git clone git@bitbucket.org:cheekymonkeymedia/monkey-wrench.git`

https://bitbucket.org/cheekymonkeymedia/monkey-wrench

From this cloned repo you can use `git pull master` from time to time, to update to the lastest code.

###Make mw executeable!####

`cd monkey-wrench`

`chmod +x mw`

###Add mw to your PATH so it can be executed from anywhere.####

Add symlink to `mw` to an existing path.
i.e: `ln -s [path/to/monkey-wrench/mw] /usr/local/bin/mw`

_(May vary depending on your OS)_

OR

Add monkey-wrench to your .bash_profile.
i.e: `export PATH="$HOME/Sites/monkey-wrench:$PATH"`

##Use

###mw

The `mw` file is a utility for running typical commands locally, and inside the docker container we use for development. For example `mw drush cc all` will run `drush cc all` inside the container.

It also acts as an interface to more complex scripts like deploy and panthon setup.

Type `mw` for a list of commands and their use.

Type `mw [command] help` for specific command help.

### Deploy

USE: mw deploy [env]
Default [env] options: [develop|stage|master]
  mw deploy develop [options]
  mw deploy [stage|master] x.x.x.x [options] - Stage or Master require version.
OPTIONS:
  -b  - will trigger a build script <project>/scripts/build if it exists.
  -*  - Any other options (-c -d -e ...) will be passed to the build script.

More Info:
https://docs.google.com/a/cheekymonkeymedia.ca/document/d/1bbIptM87S_vTPQ1yzpTqJ06G-wkozEwW-oyGESleRAg/edit?usp=sharing

### External Deployment (Where we can't git push to host.)
Set up a host remote - a duplicate of origin.
```
git remote add host [git url]
```

Copy "deploy" file by running `mw copy deploy <project>/scripts/`.

See the copied deploy file for info about how to configure with sync_command for master = git_ftp.
More info here: https://docs.google.com/a/cheekymonkeymedia.ca/document/d/1bbIptM87S_vTPQ1yzpTqJ06G-wkozEwW-oyGESleRAg/edit?usp=sharing

Install git-ftp https://github.com/git-ftp/git-ftp/blob/master/INSTALL.md

Create the file [repo]/data/private/secrets.json with the following format

{
  "ftp_username_[$env]": "",
  "ftp_password_[$env]": "",
  "ftp_address_[$env]": "(s)ftp://[address]:[port]",
  "ftp_path_[$env]": "[path to deploy files to. Leave blank if it's the location when you log in]",
  "git_ftp_syncroot": "[path to code you want to actually deploy, should be web]"
}

repeat as necessary for each env that externally deploys.

Add the contents of secrets.json to the client summary sheet for easy setup.


###Default Files

There are some default file templates you can use to start or configure a project.

These can be added to your project with the `mw copy [asset] [destination]` command.

####Docker Compose

The `docker-compose.yml` file is the default configuration for docker containers for local development.

Copy to base of your repo.

`mw copy docker [path]`
i.e.

`cd ~/Sites/my-project/cms`

`mw copy docker .`

OR simply:

`mw copy docker ~/Sites/my-project/cms`

To spin up your environment:

Configure `docker-compose.yml` for your project's php and drupal version, as well as additional utility containers.

`docker-compose up -d` From the directory where you copied your docker-compose.yml file.

Assumes project docroot is in web/ in the same directory. If your docroot is named something else because of your hosting environment, you may make a symlink called web to your docroot.

NOTE: You must spin down any other running projects before you spin up a new one to work on.
Once containers are built they are very fast to spin up and down to switch between projects.

####.gitignore

Contains some development related items that can be added to the project .gitignore.

Copy to base of your repo.

`mw copy gitignore [path]`

Will copy a gitignore if none exists, or append to an existing file.

####theme-install
If on a torsion project, use `mw theme-install default` to intitial the theme (install bower, ruby, node modules)

If on a zurb foundation 5 project that has a UI folder, simply use `mw theme-install legacy`

If on a zurb foundation 5 project that does not have a UI folder, do the following:
Run `mw copy legacy-theme-ini .`
Update the new file legacy-theme.ini to the path to your theme folder (leave a trailing blank line at the end of the file).
Now using `mw theme-install legacy`, as well as `mw grunt` will use your new theme path.

####Local Dev

local-dev.sh is a script that contains some project set up commands that can be configured for your project and comitted to the repo.

Run: `mw local-dev`

Monkey Wrench will detect an existing local-dev.sh file and copy the default into your project if none exists.

Configure the local-dev.sh file for your project and run `mw local-dev` again any time to execute the script.

OR Manually copy to `scripts/` directory in the base of your repo.

`mw copy local-dev [path]`

Will copy a default local-dev.sh to your project.

See in file comments for more information.

####Settings

For drupal projects `settings.local.php` will contain database connection settings. The default file will hold the default db connection info for our mariadb docker container and drupal database, and some default site settings. Just copying this file to your project should allow your drupal project to connect. (Unless you changed the db config in the docker-compose.yml file.)

Copy to your project [docroot]/sites/default

`mw copy settings [path]`

####D8 development services.yml

For drupal 8 projects `services.local.yml` will contain development environment specific configurations. The default file will hold the default db connection info for our mariadb docker container and drupal database. Just copying this file to your project will turn on virtually all development settings, disable db caching, and enable twig debug

Copy to your project [docroot]/sites/default

`mw copy services [path]`
