<?php

/**
 * @file: This file provides a default starting point for settings.local.php
 *
 * We will try to add more useful default configuration options to help speed
 * up local project setup.
 *
 * If you find common settings you use locally on many projects, please
 * feel free to add them to the project and create a PR for review.
 *
 * https://bitbucket.org/cheekymonkeymedia/monkey-wrench
 */

/**
 * If the code below doesn't already exist in settings.php, copy below to
 * the bottom of settings.php so it will load settings.local.php for overrides.
 * Be sure to uncomment this code in settings.php.
 * The code is here for reference and can be removed from settings.local.php.
 *
 * Do NOT commit settings.local.php to the repo. Make sure it's added to the .gitignore file.
 */
// $local_settings = dirname(__FILE__) . '/settings.local.php';
// if (file_exists($local_settings)) {
//   include $local_settings;
// }

// Keep these database settings in the settings.local.php file.
// These values should be correct for the default mariadb container settings.
// Update if your db configuration is different.

$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'drupal',
  'username' => 'drupal',
  'password' => 'drupal',
  'host' => 'mariadb',
  'prefix' => '',
);

// Turn off performance/caching.
$conf['cache'] = 0;
$conf['block_cache'] = 0;
$conf['page_cache_maximum_age'] = 0;
$conf['cache_lifetime'] = 0;
$conf['page_compression'] = 0;
$conf['preprocess_css'] = 0;
$conf['preprocess_js'] = 0;

// Other local conf and ini settings can go below.
$base_url = "http://localhost:8000";

/* Temp and Private dirctories. */
$conf['file_temporary_path'] = '../data/tmp';
$conf['file_private_path'] = '../data/private';

/* Stage File Proxy Settings */
// $conf['stage_file_proxy_origin'] = '[origin site]';
// $conf['stage_file_proxy_use_imagecache_root'] = FALSE;
// $conf['stage_file_proxy_hotlink'] = TRUE;

/* Reroute Email Settings */
// $conf['reroute_email_enable'] = 1;
// $conf['reroute_email_address'] = "[your_email]";
// $conf['reroute_email_enable_message'] = 1;
