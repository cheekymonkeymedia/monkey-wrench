#!/bin/bash

# Get the directory where this script is running.
# DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
DIR=$1
COMMAND=$2

# How to use.
if [[ $COMMAND == 'help' || $COMMAND == '' ]] ; then
  clear
  echo
  echo "There are a mix of commands and manual steps to take to complete setup of a Pantheon site on Bitbucket."
  echo -e "Follow these steps in order, commandline commands will be in \033[33mYellow\033[0m."
  echo "It would be helpful if terminus is installed on your machine, but not required."
  echo
  echo "Return to this list of steps any time you need to see your next step."
  echo
  echo -e "\033[0;32mFOR NEW PANTHEON PROJECT\033[0;0m"
  echo
  echo -e "STEP 0: \033[0;33mCreate new project on pantheon.\033[0;0m"
  echo -e "STEP 1: \033[0;33mFollow steps below to update the project.\033[0;0m"
  echo
  echo -e "\033[0;32mTO UPDATE EXISTING PANTHEON PROJECT\033[0;0m"
  echo
  echo "STEP 0: Create a new project directory."
  echo
  echo "If you have already cloned the repo somewhere else you can move it to a cms/ directory in the new project directory."
  echo "~/Sites/[project]/cms/[.git and repo files]"
  echo
  echo "This is so the setup script knows where everything is. You can move it back later if you wish."
  echo
  echo -e "STEP 1: \033[33mmw setup docroot\033[0m :: This command will help you set up the Pantheon docroot."
  echo
  echo "STEP 2: Get all CMS files moved to web docroot locally and push to pantheon."
  echo -e "     a: \033[33mmw setup move\033[0m :: This command will help you move CMS files to the new web/ directory."
  echo "     b: Verify that ALL appropriate files for the CMS were moved to the web/ directory."
  echo -e "     c: \033[33mmw setup host\033[0m :: This command will commit and push directory changes to host."
  echo
  echo "STEP 3: Verify that the changes to the file structure have been pushed to Pantheon dev. Test and move to Live if OK."
  echo
  echo "STEP 4: BitBucket repo."
  echo "     a: Create an empty https://bitbucket.com repo for this project."
  echo "     b: Copy the git URL from the overview page for the project to use in the next step."
  echo
  echo -e "STEP 5: \033[33mmw setup branch\033[0m :: This command will create new branches and multidev environments, and push the repo to Bitbucket."
  echo
  echo -e "STEP 6: \033[33mmw setup docker\033[0m :: Move docker-compose.yml scripts to project repo."
  echo
fi

if [[ $COMMAND == 'docroot' ]]; then
  echo
  echo "This process will attempt to detect and update your directory structure for a web/ docroot for Pantheon hosting."
  echo "The new directory structure will move your CMS to a web/ directory inside the git repository. Like this:"
  echo "."
  echo ".git"
  echo "web/"
  echo "web/[all cms files]"
  echo "pantheon.yml"
  # Clone or get latest master.
  if [[ -d $DIR/cms ]] ; then
    echo
    echo "cms/ directory already exists. Pulling latest master."

    cd $DIR/cms
    git checkout master
    branch_name=$(git branch | grep "*")
    branch_prefix="* "
    branch_name=${branch_name#$branch_prefix}

    if [[ $branch_name == 'master' ]] ; then
      git pull
    else
      echo
      echo -e "\033[0;31mcms/ doesn't seem to contain a git repo. Please delete or move cms/ and try again.\033[0m\n"
      exit 1
    fi
  else
    echo
    echo "What's the git url of the project you want to set up?"
    read branch
    echo
    echo "Cloning $branch"
    git clone $branch cms
  fi

  # Prep web docroot.
  if [[ -d $DIR/cms ]] ; then
    echo
    echo "Renaming git remote origin to host."
    cd $DIR/cms
    git remote rename origin host
    # Check for, or create pantheon.yml.
    echo
    echo "Checking for pantheon.yml"
    [[ -f $DIR/cms/pantheon.yml ]] && {
      echo
      echo -e "pantheon.yml exists. Please check that file for \033[33mweb_docroot: true\033[0m at the top of the file (after api_version)."
      echo -e "If this already exists and you already have a \033[33mweb/\033[0m directory at the base of the repo that holds your cms code,"
      echo "this phase is complete."
      echo
      echo -e "If not, add the line \033[33mweb_docroot: true\033[0m to the top of the file after \033[33mapi_version: 1\033[0m"
      echo -e "Then \033[33mgit add pantheon.yml\033[0m, \033[33mgit commit\033[0m and \033[33mgit push host\033[0m (remember we renamed the remote \033[7morigin\033[0m to \033[7mhost\033[0m.)"
      echo
      echo -e "\033[7mpantheon.yml file contents:\033[0m\033[30;48;5;27m"
      cat $DIR/cms/pantheon.yml
      echo -e "\033[0m"
      exit
    } || {
      echo
      echo "File pantheon.yml not found. Creating..."
      cat >$DIR/cms/pantheon.yml <<EOL
api_version: 1

web_docroot: true

EOL

      cat $DIR/cms/pantheon.yml
    }

    echo
    echo "Created pantheon.yml."
    git add pantheon.yml
    echo
    echo "Committing with message: Adding pantheon.yml to set up docroot."
    git commit -m "Adding pantheon.yml to set up docroot."
    echo
    echo "Pushing to pantheon."
    git push host master
    echo
    echo -e "Next Step: \033[0;33mmw setup move\033[0;0m"
  fi
fi

#Move files to new docroot.
if [[ $COMMAND == 'move' ]]; then
  # Check for web/ directory.
  [[ -d $DIR/cms/web ]] && {
    echo
    echo -e "\033[0;31mThe new docroot directory web/ already exists.\033[0;0m"
    echo "This process will quit."
    echo "You should manually review your files and ensure your CMS files are in the web/ docroot."
    echo "Your repo structure should look like this:"
    echo "."
    echo ".git"
    echo "web/ <- contains all cms files"
    echo "pantheon.yml"
    echo
    exit 1
  } || {
    echo
    echo -e "We need to move files to a new \033[33mweb/\033[0m directory."
    echo -e "We can attempt to do that automatically.  Choose the \033[33mcorrect\033[0m CMS to continue."
    echo "0) Cancel"
    echo "1) Drupal 7"
    echo "2) Drupal 8"
    echo "3) Wordpress"
    echo
    read cms
    case $cms in
      0)
        echo "Exiting"
        exit
        ;;
      1)
        cd $DIR/cms/
        echo "Moving Drupal 7 files."
        mkdir -p web && git mv -k $(find . -type f -maxdepth 1 | grep -v pantheon.yml) includes/ misc/ modules/ profiles/ scripts/ sites/ themes/ web
        ;;
      2)
        cd $DIR/cms/
        echo "Moving Drupal 8 files."
        mkdir web && git mv -k $(find . -type f -maxdepth 1 | grep -v pantheon.yml) core drush modules profiles sites themes vendor web
        ;;
      3)
        cd $DIR/cms/
        echo "Moving Wordpress files."
        mkdir web && git mv -k $(find . -type f -maxdepth 1  | grep -v pantheon.yml) wp-includes wp-content wp-admin web
        ;;
      *)
        echo "Invalid selection. Exiting."
        exit
        ;;
    esac
    echo -e "Attempted to move CMS to web/ directory.  \033[31mPlease confirm that all CMS files have been moved.\033[0m"
    echo "Your git root directory may contain other development related files, but the CMS should now live in web/."
    echo
    echo -e "Once you are sure all the correct files are in the web/ directory, run \033[33mmw setup host\033[0m"
    echo "to commit the changes and push to the host."
    exit
  }
fi

#Commit and push changes to host.
if [[ $COMMAND == 'host' ]] ; then
  cd $DIR/cms
  echo
  echo "Creating basic .gitignore rules."
  [[ -f $DIR/cms/.gitignore ]] && {
    echo -e "\033[31m.gitignore file aready exists. Skipping...\033[0m"
  } || {
    cat >$DIR/cms/.gitignore <<EOL
# IDE artifacts
*~
\#*\#
*.swp
.DS*
.project
.idea
*.sublime*

docker-runtime/

EOL
    git add .gitignore
    git commit -m "Add basic .gitignore for git root." .gitignore
  }
  echo
  echo "Adding and committing directory changes to the repo."
  git add .
  git commit -m "Committing new directory structure for web/ docroot."
  echo
  echo "Pushing to host."
  git push host master
  echo
  echo -e "\033[32mNEXT STEPS\033[0m"
  echo
  echo "STEP 3: Verify that the changes to the file structure have been pushed to Pantheon dev. Test and move to Live if OK."
  echo "STEP 4a: Create an empty https://bitbucket.com repo for this project."
  echo "STEP 4b: Copy the git URL from the overview page for the bitbucket repo to use in the next step."
  echo -e "STEP 5: \033[33mmw setup branch\033[0m :: This command will create new branches and multidev environments, and push the repo to Bitbucket."
fi

#Create branches and set up multidev environments.
if [[ $COMMAND == 'branch' ]] ; then
  cd $DIR/cms
  git checkout master
  echo
  echo "Creating develop branch and pushing to host."
  git branch develop
  git push host develop
  echo
  echo "Creating stage branch and pushing to host."
  git branch stage
  git push host stage

  ###Set Up Bitbucket
  echo
  echo "Paste the git url for your bitbucket repo:"
  read bitbucket
  if [[ $bitbucket == '' ]] ; then
    echo "No git URL added, please add remote and push manually with the following steps."
    echo "STEP 1: Set Up an empty bitbucket repo for this project. Copy just the git:// URL"
    echo -e "STEP 2: \033[33mgit remote add origin [git url (bitbucket)]\033[0m :: (add bitbucket git url as remote origin)"
    echo -e "STEP 3: \033[33mgit push -u origin develop\033[0m :: push develop branch to bitbucket (origin) and add 'upstream (-u)'."
    echo "Pushing develop first sets develop as default branch on bitbucket."
    echo -e "\033[33mgit push -u origin master\033[0m :: push master to bitbucket."
  else
    echo
    echo "Adding bitubcket as remote origin."
    git remote add origin $bitbucket
    echo "Pushing develop branch to origin."
    git push -u origin develop
    echo "Pushing master branch to origin."
    git push -u origin master
  fi

  #Check if Terminus 1.x is installed.
  terminus=$( terminus --version | grep "Terminus 1." )
  if [[ $terminus == '' ]] ; then
    #No terminus detected.
    echo
    echo "Terminus 1.x was not detected. Create your develop and stage environments as follows."
    echo -e "On Pantheon Multidev > Git branches :: \033[33mCreate Environment\033[0m on the \033[33mdevelop\033[0m branch from \033[33mlive\033[0m."
    echo -e "On Pantheon Multidev > Git branches :: \033[33mCreate Environment\033[0m on the \033[33mstage\033[0m branch from \033[33mlive\033[0m."
  else
    #Use Terminus to create multidev envs from develop and stage branches.
    echo
    terminus site:list --field=name
    echo
    echo "Terminus detected. Please copy/paste the exact name of the site you wish to update from the list above."
    echo "If you don't see the site name, make sure the site exists, you have access, and terminus is connected to the correct account."
    read sitename
    if [[ $sitename == '' ]] ; then
      echo -e "\033[31mNo site name selected. Exiting.\033[0m"
      exit 1
    fi
    echo
    echo "Attempting to set up develop multidev for $sitename on Pantheon with Terminus"
    terminus multidev:create $sitename.dev develop &>/dev/null &
    echo
    echo "Attempting to set up stage multidev for $sitename on Pantheon with Terminus"
    terminus multidev:create $sitename.dev stage &>/dev/null &
  fi
  echo
  echo -e "NEXT STEP: Copy docker-compose.yml file. \033[33mmw setup docker\033[0m"
fi
# Move the toolkit.
if [[ $COMMAND == 'docker' ]] ; then
  echo
  echo -e "This should be done last. Move docker-compose.yml to \033[33m$DIR/cms/\033[0m? [y/N]"
  read docker
  if [[ $docker == 'y' ]] ; then
    echo
    echo "Copying docker-compose.yml to $DIR/cms/"
    mw copy docker $DIR/cms
  else
    echo
    echo "Skipping"
    echo -e "You can use \033[33mmw copy docker [path]\033[0m to copy docker-compose.yml to any directory."
  fi
  echo
  echo -e "For other default local configuration files see: \033[33mmw copy help\033[0m"
  echo
  mw copy help
fi
