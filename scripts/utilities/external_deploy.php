#!/usr/bin/php
<?php
include_once 'get_secrets.inc';

$arguments = arguments($argv);

if (empty($arguments['options']['env'])) {
  echo "no envrionment specified!\n";
  return 1;
}

$env = $arguments['options']['env'];
$initial_deploy = !empty($arguments['options']['initial_deploy']) ? $arguments['options']['initial_deploy'] : 'n';

$branch = '';
switch ($env) {
  case 'develop':
    $branch = $env;
    break;
  case 'prod':
  case 'master':
    $branch = 'master';
    break;
  default:
    // Staging is special, we get the branch, not env.
    if (strpos($env, 'staging/') !== FALSE) {
      // Set the branch to the argument passed,
      // then set $env back to stage.
      $branch = $env;
      $env = 'stage';
    }
}

$u_key = 'ftp_username_' . $env;
$p_key = 'ftp_password_' . $env;
$address_key = 'ftp_address_' . $env;
$path_key = 'ftp_path_' . $env;
$syncroot_key = 'git_ftp_syncroot';
$insecure_key = 'git_ftp_insecure';

// Load our hidden credentials.
$secrets = _get_secrets(array($u_key, $p_key, $address_key, $path_key));

if (!empty($secrets[$u_key]) && !empty($secrets[$p_key]) && !empty($secrets[$address_key])) {
  $command = '';

  // Run the actual deploy if we have the commands set.
  if ($branch) {
    if ($initial_deploy === 'y') {
      $action = 'git ftp init';
    }
    else {
      $action = 'git ftp push';
    }

    $command = $action . ' -v --insecure -u "' . $secrets[$u_key] . '" -p "' . $secrets[$p_key] . '" -b "' .
     $branch . '" ' . $secrets[$address_key] . $secrets[$path_key];

    // Sync an alternate root (not the repo root).
    if (!empty($secrets[$syncroot_key])) {
      $command .= ' --syncroot "' . $secrets[$syncroot_key] . '"';
    }

    // Add --insecure flag?
    if (!empty($secrets[$insecure_key])) {
      $command .= ' --insecure';
    }

    echo "running git deploy script:\n$command\n";

    // end all output buffers if any.
    while (@ ob_end_flush());

    // Echo output from command back to screen as they happen.
    $proc = popen($command, 'r');
    while (!feof($proc)) {
      echo fread($proc, 4096);
      @ flush();
    }
    pclose($proc);
  }
}
else {
  if (empty($secrets[$u_key])) {
    echo "no username for $env\n";
  }
  if (empty($secrets[$p_key])) {
    echo "no password for $env\n";
  }
  if (empty($secrets[$address_key])) {
    echo "no address for $env\n";
  }
}

/**
 * Parse the arguments passed to the script.
 */
function arguments($args) {
  array_shift($args);
  $endofoptions = false;

  $ret = array(
    'commands' => array(),
    'options' => array(),
    'flags' => array(),
    'arguments' => array(),
  );

  while ($arg = array_shift($args)) {
    // if we have reached end of options,
    // we cast all remaining argvs as arguments
    if ($endofoptions) {
      $ret['arguments'][] = $arg;
      continue;
    }

    // Is it a command? (prefixed with --)
    if (substr($arg, 0, 2) === '--') {
      // is it the end of options flag?
      if (!isset($arg[3])) {
        $endofoptions = true;
        ; // end of options;
        continue;
      }

      $value = "";
      $com = substr($arg, 2);

      // is it the syntax '--option=argument'?
      if (strpos($com, '=')) {
        list($com, $value) = explode("=", $com, 2);
      }
      // is the option not followed by another option but by arguments
      elseif (strpos($args[0], '-') !== 0) {
        while (strpos($args[0], '-') !== 0)
          $value .= array_shift($args) . ' ';
        $value = rtrim($value, ' ');
      }

      $ret['options'][$com] = !empty($value) ? $value : true;
      continue;
    }

    // Is it a flag or a serial of flags? (prefixed with -)
    if (substr($arg, 0, 1) === '-') {
      for ($i = 1; isset($arg[$i]); $i++)
        $ret['flags'][] = $arg[$i];
      continue;
    }

    // finally, it is not option, nor flag, nor argument
    $ret['commands'][] = $arg;
    continue;
  }

  if (!count($ret['options']) && !count($ret['flags'])) {
    $ret['arguments'] = array_merge($ret['commands'], $ret['arguments']);
    $ret['commands'] = array();
  }
  return $ret;
}
