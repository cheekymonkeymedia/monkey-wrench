#!/bin/bash
DIR=$1

# Fetch cores from drupal search_api_solr.
if [[ -d $DIR/assets/solr ]] ; then
  echo
  echo "Solr assets detected. Skipping clone."
else
  echo "Which Drupal? [7|8 coming soon]"
  read drupal

  case $drupal in
    7)
      solr_version=7.x-1.11
      echo
      echo "Cloning search_api_solr-$solr_version..."
      git clone --branch 7.x-1.x https://git.drupal.org/project/search_api_solr.git assets/solr/search_api_solr
      ;;
    # 8)
    #   solr_version=8.x-1.0-beta1
    #   echo
    #   echo "Cloning search_api_solr-$solr_version..."
    #   git clone --branch 8.x-1.x https://git.drupal.org/project/search_api_solr.git assets/solr/search_api_solr
    #   ;;
    *)
      echo
      echo "Invalid drupal version. Exiting."
      echo
      exit 1
      ;;
  esac

  cd $DIR/assets/solr/search_api_solr
  git checkout $solr_version
  echo
  echo "Extracting solr-conf..."
  cp -r solr-conf ../
  cd ../
  echo
  echo "Cleaning up..."
  rm -rf search_api_solr
  cat > $DIR/assets/solr/VERSION.txt <<EOF
Drupal: search_api_solr-$solr_version
EOF

fi

echo
echo Please input a name for creating a solr core:
read core_name
docker-compose exec solr sh -c "cd /opt/solr; solr create -c $core_name"

# We need to figure out what version of search_api config file to copy.
echo
echo "Which version of Search API should be configured?"
echo "1] 5.x"
echo
read solr
case $solr in
  1 )
    solr_conf="5.x"
    ;;
  * )
    echo
    echo "No version selected. Exiting."
    exit 1
    ;;
esac

docker-compose exec solr sh -c "cp -rf /opt/solr/server/solr/local/assets/solr/solr-conf/$solr_conf/. /opt/solr/server/solr/$core_name/conf/" && \
echo "Search_api $solr_conf conf files have been copy to solr core $core_name successfully."

# Solr container ip addressed is needed for search-api connection. So first grab the solr container_id
container_id=$(docker ps -qf "name=solr");
solr_cont_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $container_id);

search_api_server_info="\n
====================================================================\n
Please update your search_api server with the following information:\n
Go to http://localhost:8000/admin/config/search/search_api\n
Solr host: $solr_cont_ip\n
Solr port: 8983\n
Solr path: /solr/$core_name\n
====================================================================\n";

# Since we create a new core, we have to reload this core, otherwise drupal will complain we used the wrong schema.xml.
{
  curl "http://localhost:8983/solr/admin/cores?action=RELOAD&core=$core_name";
} &> /dev/null &&  #since this command will output stdout, and we don't want to confused anyone, so lets hide it.
echo "Solr core $core_name successfully reloaded."

# Ouput setup informations.
echo -e "$search_api_server_info"
echo "The above information also save in $DIR/assets/solr/search_api_server_info.txt"

# Save this information to a file as a reference.
cat > $DIR/assets/solr/search_api_server_info.txt <<EOF
`echo -e $search_api_server_info`
EOF
